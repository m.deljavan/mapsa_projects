// take a string of numbers
const input = prompt('Please insert your numbers');
// create a array from taken input
const arrNumbers = input.split('-');

let sumOfNumbers = 0;
let minNumber = 100000000000000000;
let maxNumber = 0;
arrNumbers.forEach(num => {
    sumOfNumbers += +num;
    if (num > maxNumber) {
        maxNumber = +num
    }
    if (num < minNumber) {
        minNumber = +num
    }
})

const average = sumOfNumbers / arrNumbers.length;

console.log (`maximum number is ${maxNumber}`);
console.log (`minimum number is ${minNumber}`);
console.log (`average  is ${average}`);
