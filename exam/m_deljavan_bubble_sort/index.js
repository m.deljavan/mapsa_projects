const numbers_string = prompt('Please insert numbers:');
const numbers_arr = numbers_string.split('-').map(num => +num);

const swap = (arr, index1, index2) => {
    const first = arr[index1];
    arr[index1] = arr[index2];
    arr[index2] = first;
}
let is_swap = false;
do {
    is_swap = false;
    for (let i = 0; i < numbers_arr.length - 1; i++) {
        if (numbers_arr[i] > numbers_arr[i + 1]) {
            is_swap = true;
            swap(numbers_arr, i, i + 1);
        }
    }
} while (is_swap)

console.log(numbers_arr.join('-'))